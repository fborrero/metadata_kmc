<?php

/**
 * @file
 * Kaltura Metadata pages generation functions.
 */

function metadata_kmc_get_custom_metadata_content($entry_id) {
  $data = array();

  $entry_id = trim($entry_id);

  $data['id'] = $entry_id;

  if ($kaltura_entry = kaltura_entry_load($entry_id)) {
    $data['title'] = '';
    if (!empty($kaltura_entry->kaltura_title)) {
      $data['title'] = $kaltura_entry->kaltura_title;
    }

    $data['success'] = TRUE;
  }
  else {
    $data['success'] = FALSE;
    $data['errorMessage'] = t("Kaltura entry not found.");
  }

  if (empty($data['success'])) {
    $data['success'] = FALSE;
    $data['error'] = TRUE;

    if (empty($data['errorMessage'])) {
      $data['errorMessage'] = t("Error unknown.");
    }
  }

  // return drupal_json_encode($data);
  drupal_json_output($data);
}
