(function ($) {

    Drupal.behaviors.metadata_kmc = {
        attach: function (context, settings) {
            var mediaTypes = {};
            mediaTypes.Video = 1;
            mediaTypes.Image = 2;
            mediaTypes.Audio = 5;
            $('.kentry_add', context).click(function () {
                var $customMetadata = $('#custom_meta_edit');
                var is_edit = $customMetadata.hasClass("custom_meta_edit");

                if (is_edit) {
                    var entry_id = $(this).attr('id');

                    // OLD BEHAVIOR
                    // var is_visible = $("#custom_meta_edit").is(":visible");
                    // // console.log(is_visible);
                    // if(!is_visible){
                    //     $("#custom_meta_edit").show('slow');
                    // }

                    // //jQuery("#custom_meta_edit").load("/metadata/get/ajax/"+entry_id);
                    // // console.log("1...");

                    // $.ajax({
                    //     url: settings.basePath + '?q=metadata/get/custom_metadata/' + entry_id,
                    //     error: function (response){
                    //       // console.log("error");
                    //     },
                    //     success: function(result){
                    //         $("#custom_meta_edit").replaceWith(result);

                    //         $("#custom_meta_edit").siblings('input[name="form_build_id"]').remove();
                    //         $("#custom_meta_edit").siblings('input[name="form_token"]').remove();
                    //         $("#custom_meta_edit").siblings('input[name="form_id"]').remove();

                    //         Drupal.attachBehaviors();
                    //     }
                    // });

                    // NEW BEHAVIOR by Micoworker
                    $.ajax({
                        url: settings.basePath + '?q=metadata-kmc/get-custom-metadata/' + entry_id,
                        error: function (jqXHR, textStatus, errorThrown){
                            // console.log("error");
                            // console.log(jqXHR);
                        },
                        success: function(data, textStatus, jqXHR){
                            // console.log("success");
                            // console.log(data);
                            if (data.success) {
                                // Title
                                var $title = $customMetadata.find('.metadata-kmc-name-field');
                                if ($title.val().trim() == '') {
                                    $title.val(data.title);
                                }
                            }
                        },
                        complete: function(jqXHR, textStatus) {
                            $customMetadata.show();
                        },
                    });
                }
            });
        },
    };

})(jQuery);
