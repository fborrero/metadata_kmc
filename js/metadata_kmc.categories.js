(function ($) {

    Drupal.behaviors.metadata_kmc_categories = {
        attach: function (context, settings) {

          $('.metadata-kmc-categories-field .form-checkboxes').once('metadata-kmc-categories-filter', function () {
            $(this).before('<div class="categories-filter-wrapper"><input class="form-text categories-filter" type="textfield" placeholder="Filter categories"></div>');
          });

          $('.categories-filter').keyup(function () {
            metadata_kmc_filter_categories();
          });

        }
    };

    metadata_kmc_filter_categories = function () {
      var filter = $('.categories-filter').val().trim().toLowerCase();
      var $categories = $('.metadata-kmc-categories-field .form-checkboxes input[type="checkbox"]');

      $categories.each(function(index, el) {
        var category = $(this);
        var name = category.siblings('label').text().toLowerCase();
        if (name.indexOf(filter) !== -1) {
          category.parent().show();
        }
        else {
          category.parent().hide();
        }
      });
    };

})(jQuery);
